<?php
// $Id$

/**
 * @file
 * Main module setup.
 *
 * Has the main content that is being configured for the status lights module.
 * Includes block creation, helper functions, and menu creation.
 */

/**
 * Implements hook_menu().
 */
function status_lights_menu()
{
  $items = array();
  $base = array(
    'access callback'   =>      'user_access',
    'access arguments'  =>      array('administer status light'),
    'file'              =>      'status_lights_admin.inc',
  );
  $items['admin/structure/statuslights'] = $base + array(
    'title'             =>      'Status Lights',
    'description'       =>      'Configuration settings for the Status Lights module.',
    'type'              =>      MENU_NORMAL_ITEM,
    'page callback'     =>      'status_lights_config_page',
    'page arguments'    =>      array(1),
  );
  $items['admin/structure/statuslights/list'] = $base + array(
    'title'             =>      'List',
    'page callback'     =>      'status_lights_config_page',
    'page arguments'    =>      array(1),
    'description'       =>      'See all the indicator lights available and modify them as necessary.',
    'type'              =>      MENU_DEFAULT_LOCAL_TASK,
    'weight'            =>      '-1',
  );
  $items['admin/structure/statuslights/manual'] = $base + array(
    'title'             =>      'Manually Set',
    'description'       =>      'Manually set the indicator light status.',
    'page callback'     =>      'status_lights_manual_list',
    'page arguments'    =>      array(1),
    'type'              =>      MENU_LOCAL_TASK,
    'weight'            =>      1,
  );
  $items['admin/structure/statuslights/manual/%idloader/%'] = $base + array(
    'title'             =>      'Manual Set Link',
    'description'       =>      'Manually setting the set indicator to whatever status is chosen',
    'page callback'     =>      'status_lights_manual_set',
    'page arguments'    =>      array(4,5),
    'type'              =>      MENU_NORMAL_ITEM,
  );
  $items['admin/structure/statuslights/config'] = $base + array(
    'title'             =>      'Configuration',
    'description'       =>      'Global config settings for the Status Lights module',
    'page callback'     =>      'drupal_get_form',
    'page arguments'    =>      array('status_lights_config_global_form'),
    'type'              =>      MENU_LOCAL_TASK,
    'weight'            =>      2,
  );
  $items['admin/structure/statuslights/add'] = $base + array(
    'title'             =>      'Add Indicator',
    'description'       =>      'Add a new status light indicator',
    'page callback'     =>      'drupal_get_form',
    'page arguments'    =>      array('status_lights_form'),
    'type'              =>      MENU_LOCAL_ACTION,
  );
  $items['admin/structure/statuslights/delete/%idloader'] = $base + array(
    'title'             =>      'Delete',
    'description'       =>      'Delete the selected status indicator light',
    'page callback'     =>      'status_lights_delete',
    'page arguments'    =>      array(4),
    'type'              =>      MENU_NORMAL_ITEM,
  );
  $items['admin/structure/statuslights/edit/%idloader'] = $base + array(
    'title'             =>      'Edit',
    'description'       =>      'Modify the selected status indicator light',
    'page callback'     =>      'drupal_get_form',
    'page arguments'    =>      array('status_lights_form', 4),
    'type'              =>      MENU_NORMAL_ITEM,
  );
  $items['admin/structure/statuslights/change/%idloader'] = $base + array(
    'title'             =>      'Change Status',
    'description'       =>      'Change the status of the indicator',
    'page callback'     =>      'status_lights_change_status',
    'page arguments'    =>      array(4),
    'type'              =>      MENU_NORMAL_ITEM,
  );
  return $items;
}

/**
 * Implements hook_load().
 */
function idloader_load($id)
{
  return $id;
}

/**
 * Implements hook_to_arg().
 */
function idloader_to_arg($id)
{
  return $id;
}

/**
 * Implements hook_permission().
 */
function status_lights_permission()
{
  return array(
    'administer status light' => array(
      'title'       => t('Administer status lights'),
      'description' => t('Allows the user the change settings of the status light indicators'),
    ),
  );
}

/**
 * Implements hook_block_info().
 */
function status_lights_block_info()
{
	$indicators = db_select('status_indicator', 's')->fields('s')->condition('s.enabled', 1)->execute()->fetchAll();
	$blocks = array();
	foreach($indicators as $light)
  {
		$key = "light-" . $light->iid;
  	$blocks[$key] = array(
    	'info'  => t('Status Lights - ' . $light->name),
    	'cache' => DRUPAL_NO_CACHE,
  	);
	}
  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function status_lights_block_view($delta)
{
	$iid = substr($delta, 6);
  require_once("status_lights_admin.inc");
  drupal_add_css(drupal_get_path('module', 'status_lights') . '/css/status_lights_style.css', array('group' => CSS_DEFAULT, 'every_page' => TRUE));
  $block['subject'] = t('Status Light');
	$indicators = db_select('status_indicator', 's')->fields('s')->condition('s.iid', $iid)->condition('s.enabled', 1)->orderBy('s.weight', 'ASC')->execute()->fetchAll();
  $content = "";
  foreach($indicators as $light)
  {
		$link = $light->name;
		if($light->content_link != "#")
		{
			$link = "<a href='" . $light->content_link . "'>" . $light->name . "</a>";
		}
    $content .= "<div class='indicator'>";
    $content .= "<img src='" . status_lights_get_status_image($light->iid) . "'>$link<br>";
    $content .= "<span class='hours'>" . status_lights_get_hours($light->iid) . "</span>";
    $content .= "</div>";
  }
  $block['content'] = $content;
  return $block;
}
