<?php
// $Id$

/**
 * @file
 * Status Lights admin functions.
 *
 * Contains the main functions used, including forms and helper functions.
 */

/**
 * Function that will manually set the indicator status.
 *
 * Updates the database with the status passed as an argument ($status).
 *
 * @param $iid
 *   The indicator ID number that is being manually set to the
 *   specified status.
 * @param $status
 *   $status is the status level we are changing the indicator to.
 *   Valid values range from 0 to 3 (integers only).
 *   0 specifies that the indicator should be automated based off a schedule.
 *   1 specifies that the indicator should be GREEN (open).
 *   2 specifies that the indicator should be YELLOW (closing soon).
 *   3 specifies that the indicator should be RED (closed).
 */
function status_lights_manual_set($iid = NULL, $status = NULL)
{
  $color[] = '';
  $color[] = 'green';
  $color[] = 'yellow';
  $color[] = 'red';
  if($iid != null && $status != null)
  {
    $query = db_update('status_indicator')
    ->fields(array('manual_status' => $status))
    ->condition('iid', $iid)
    ->execute();
  }
  if($status != 0)
  {
    drupal_set_message('Indicator Status set to ' . $color[$status]);
  }
  else
  {
    drupal_set_message('Indicator Status set to update automatically based off schedule.');
  }
  drupal_goto('admin/structure/statuslights/manual');
}

/**
 * Function callback that displays a list of indicators to manually set.
 *
 * Outputs a formatted list (tabled) with options to set manual status of each
 * indicator or reset the indicator's status to 0 (automated).
 */
function status_lights_manual_list()
{
  $imagepath = variable_get('status_lights_image_path');
  $red = $imagepath . variable_get('status_lights_image_red');
  $yellow = $imagepath . variable_get('status_lights_image_yellow');
  $green = $imagepath . variable_get('status_lights_image_green');
  $indicators = db_select('status_indicator', 's')->fields('s')->orderBy('s.weight', 'ASC')->orderBy('s.name', 'ASC')->execute()->fetchAll();
  $out = "<table width='100%' cellpadding='3' cellspacing='1'>";
  $out .= "<tr>";
  $out .= "<th width='10%'>Current Status</th><th>Indicator</th><th width='25%'>Operations</th>";
  $out .= "</tr>";
  foreach($indicators as $ind)
  {
    $status = (intval($ind->enabled) == 1) ? "Disable" : "Enable";
    $out .= "<tr style='border-bottom:solid 1px #bebfb9;'>";
    $out .= "<td align='center'><img src='" . status_lights_get_status_image($ind->iid) . "'></td>";
    $out .= "<td>$ind->name";
    $out .= ($ind->description != NULL) ? "<br><em>$ind->description</em>" : "";
    $out .= "</td>";
    $out .= "<td>";
    $out .= "<a style='padding-right:20px;' href='manual/$ind->iid/1'><img src='$green'></a>";
    $out .= "<a style='padding-right:20px;' href='manual/$ind->iid/2'><img src='$yellow'></a>";
    $out .= "<a style='padding-right:20px;' href='manual/$ind->iid/3'><img src='$red'></a>";
    $out .= "<a style='padding-right:20px;' href='manual/$ind->iid/0'>Reset</a>";
    $out .= "</td>";
    $out .= "</tr>";
  }
  $out .= "</table>";
  return $out;
}

/**
 * Function that will set the enabled/disabled status of the indicator
 *
 * Updates the database with the enabled/disabled status for the indicator
 * based on what it currently is set.
 *
 * @param $iid
 *   $iid is the Indicator ID number that is being enabled or disabled.
 */
function status_lights_change_status($iid = NULL)
{
  if($arg != NULL)
  {
    $datas = db_select('status_indicator', 's')->fields('s', array('enabled'))->condition('s.iid', $iid)->execute()->fetchAll();
    foreach($datas as $row)
    {
      if(intval($row->enabled) == 1)
      {
        $changeto = 0;
      }
      else if (intval($row->enabled) == 0)
      {
        $changeto = 1;
      }
    }
    $update = db_update('status_indicator')->fields(array('enabled' => $changeto))->condition('iid', $iid)->execute();
  }
  drupal_goto('admin/structure/statuslights');
}

/**
 * Function that will delete an indicator
 *
 * Removes the specified indicator completely from the database
 *
 * @param $iid
 *   $iid is the Indicator ID number that is being deleted.
 */
function status_lights_delete($iid = NULL)
{
  // perform delete functions
  if($iid != NULL)
  {
    $query = db_delete('status_indicator')->condition('iid', $iid)->execute();
    drupal_set_message(t('The indicator was deleted successfully.'));
  }
  else
  {
    drupal_set_message(t('The indicator could not be deleted.'));
  }
  return "<a href='..'>Return to Indicator List</a>";
}

/**
 * Helper function to get all content types available.
 *
 * Function to return all content types in a formatted array for processing in
 * a form select list.
 *
 * @return
 *   Returns a formatted key/value options array for use in a select list.  The
 *   keys are the content type, and the values are the names of the content
 *   types.
 */
function status_lights_type_list()
{
  $options = array();
  $types = db_select('node_type', 'n')->fields('n')->orderBy('n.name', 'ASC')->execute()->fetchAll();
  foreach($types as $type)
  {
    $options[$type->type] = t($type->name);
  }
  return $options;
}

/**
 * Helper function to get all custom fields available.
 *
 * Function to return all custom fields in a formatted array for processing in
 * a form select list.
 *
 * @return
 *   Returns a formatted key/value options array for use in a select list.  The
 *   keys are the machine field names, and the values are the labels and
 *   machine field names.
 */
function status_lights_field_list()
{
  $fields = array();
  $datas = db_select('field_config_instance', 'f')->fields('f', array('field_name', 'data'))->orderBy('f.field_name', 'ASC')->distinct()->execute()->fetchAll();
  foreach($datas as $field)
  {
    $temp = $field->data;
    $data = unserialize($temp);
    $fields[$field->field_name] = $data['label'] . " ($field->field_name)";
  }
  return $fields;
}

/**
 * Helper function to get all taxonomy terms in a specific vocabulary.
 *
 * Function to return all taxonomy terms in a formatted array for processing in
 * a form select list.
 *
 * @return
 *   Returns a formatted key/value options array for use in a select list.  The
 *   keys are the taxonomy term id, and the values are the taxonomy term names.
 */
function status_lights_term_list()
{
  $terms = array();
  $tree = taxonomy_get_tree(3);
  foreach ($tree as $term)
  {
    $terms[$term->tid] = t($term->name);
  }
  return $terms;
}

/**
 * Form validation handler for status_lights_form().
 *
 * @see status_lights_form()
 * @see status_lights_form_submit()
 */
function status_lights_form_validate($form, &$form_state)
{
    
}

/**
 * Form submission handler for status_lights_form().
 *
 * @see status_lights_form()
 * @see status_lights_form_validate()
 */
function status_lights_form_submit($form, &$form_state)
{
  $found = array_key_exists('process', $form_state['values']);
  if($found)
  {
    if($form_state['values']['process'] == 'edit')
    {
      $query = db_update('status_indicator')
      ->fields(array(
        'name'          => $form_state['values']['indicator_name'],
        'description'   => $form_state['values']['indicator_desc'],
				'content_link'  => $form_state['values']['indicator_link'],
        'content_type'  => $form_state['values']['indicator_type'],
        'field'         => $form_state['values']['indicator_field'],
        'taxonomy_term' => $form_state['values']['indicator_term'],
        'show_hours'    => $form_state['values']['indicator_hours'],
        'global_time'   => $form_state['values']['indicator_global'],
        'yellowtime'    => $form_state['values']['indicator_yellow'],
        'redtime'       => $form_state['values']['indicator_red'],
      ))
      ->condition('iid', $form_state['values']['iid'])
      ->execute();
      $s = 'The indicator has been updated successfully.';
    }
  } else
  {
    $query = db_insert('status_indicator')
    ->fields(array(
      'name'           => $form_state['values']['indicator_name'],
      'description'    => $form_state['values']['indicator_desc'],
			'content_link'   => $form_state['values']['indicator_link'],
      'content_type'   => $form_state['values']['indicator_type'],
      'field'          => $form_state['values']['indicator_field'],
      'taxonomy_term'  => $form_state['values']['indicator_term'],
      'taxonomy_vocab' => 3,
      'added_time'     => time(),
      'added_by'       => '',
      'weight'         => 5,
      'manual_status'  => 0,
      'show_hours'     => $form_state['values']['indicator_hours'],
      'enabled'        => 0,
      'global_time'    => $form_state['values']['indicator_global'],
      'yellowtime'     => $form_state['values']['indicator_yellow'],
      'redtime'        => $form_state['values']['indicator_red'],
    ))
    ->execute();
    $s = 'The indicator has been added successfully.';
  }
  drupal_set_message(t($s));
}

/**
 * Form builder for the status indicator add and edit form.
 *
 * @param $iid
 *   The indicator ID to edit.
 *
 * @see status_lights_form_validate()
 * @see status_lights_form_submit()
 * @ingroup forms
 */
function status_lights_form($form, &$form_state, $iid = null)
{
  $form = array();

  $form['indicator_name'] = array(
    '#type'         =>      'textfield',
    '#title'        =>      t('Indicator Name'),
    '#description'  =>      t('Enter the name of the indicator that you would like to have shown to all users (ie: on the home page)'),
    '#required'     =>      TRUE,
  );

  $form['indicator_term'] = array(
    '#type'         =>      'select',
    '#title'        =>      t('Building'),
    '#description'  =>      t('The taxonomy term of the building this indicator pulls its schedule from'),
    '#multiple'     =>      FALSE,
    '#options'      =>      status_lights_term_list(),
    '#required'     =>      TRUE,
  );

  $form['indicator_type'] = array(
    '#type'         =>      'select',
    '#title'        =>      t('Content Type'),
    '#description'  =>      t('The content type that the indicator checks for the specified field'),
    '#options'      =>      status_lights_type_list(),
    '#required'     =>      TRUE,
  );
    
  $form['indicator_field'] = array(
    '#type'         =>      'select',
    '#title'        =>      t('Field'),
    '#description'  =>      t('The field to check for hours'),
    '#options'      =>      status_lights_field_list(),
    '#required'     =>      TRUE,
  );

	$form['indicator_link'] = array(
		'#type'         =>      'textfield',
		'#title'        =>      t('Link'),
		'#description'  =>      t('URL to that the indicator will link to.'),
		'#default_value'=>      '#',
	);
    
  $form['indicator_desc'] = array(
    '#type'         =>      'textarea',
    '#title'        =>      t('Description'),
    '#description'  =>      t('Enter the description of the indicator that will show up on the administration panel'),
    '#cols'         =>      35,
    '#rows'         =>      5,
  );
    
  $form['indicator_global'] = array(
    '#type'         =>      'checkbox',
    '#title'        =>      t('Use Global Time Limits'),
    '#description'  =>      t('Uncheck this box if you want to set your own RED and YELLOW time limits (minutes before closing that the indicator switches status automatically)'),
    '#return_value' =>      1,
  );
    
  $form['indicator_yellow'] = array(
    '#type'         =>      'textfield',
    '#title'        =>      t('Yellow Time Limit'),
    '#description'  =>      t('Time (in minutes) before closing that the indicator will show YELLOW status'),
  );
    
  $form['indicator_red'] = array(
    '#type'         =>      'textfield',
    '#title'        =>      t('Red Time Limit'),
    '#description'  =>      t('Time (in minutes) before closing that the indicator will show RED status'),
  );
    
  $form['indicator_hours'] = array(
    '#type'         =>      'checkbox',
    '#title'        =>      t('Show Hours with Light'),
    '#description'  =>      t('Check this box if you want the schedule hours to be shown alongside the indicator light'),
    '#return_value' =>      1,
  );
    
  $form['submit'] = array(
    '#type'         =>      'submit',
    '#value'        =>      t('Add Indicator'),
  );
    
  if($iid != null)
  {
    $datas = db_select('status_indicator', 's')->fields('s')->condition('s.iid', $iid)->execute()->fetchAll();
    foreach($datas as $row)
    {
      $form['indicator_name']['#default_value'] = $row->name;
      $form['indicator_term']['#default_value'] = $row->taxonomy_term;
      $form['indicator_desc']['#default_value'] = $row->description;
      $form['indicator_hours']['#default_value'] = $row->show_hours;
      $form['indicator_type']['#default_value'] = $row->content_type;
      $form['indicator_field']['#default_value'] = $row->field;
      $form['indicator_global']['#default_value'] = $row->global_time;
      $form['indicator_yellow']['#default_value'] = $row->yellowtime;
      $form['indicator_red']['#default_value'] = $row->redtime;
			$form['indicator_link']['#default_value'] = $row->content_link;
      $form['submit']['#value'] = t('Save Indicator');
      $form['process'] = array(
        '#type'  => 'hidden',
        '#value' => 'edit',
      );
      $form['iid'] = array(
        '#type'  => 'hidden',
        '#value' => $iid,
      );
    }
  }

  return $form;
}

/**
 * Form validation handler for status_lights_config_global_form().
 *
 * @see status_lights_config_global_form()
 * @see status_lights_config_global_form_submit()
 */
function status_lights_config_global_form_validate($form, &$form_state)
{
    
}

/**
 * Form submission handler for status_lights_config_global_form().
 *
 * @see status_lights_config_global_form()
 * @see status_lights_config_global_form_validate()
 */
function status_lights_config_global_form_submit($form, &$form_state)
{
  variable_set('status_lights_image_green', $form_state['values']['green_light']);
  variable_set('status_lights_image_yellow', $form_state['values']['yellow_light']);
  variable_set('status_lights_image_red', $form_state['values']['red_light']);
  variable_set('status_lights_red_time', $form_state['values']['red_time']);
  variable_set('status_lights_yellow_time', $form_state['values']['yellow_time']);
  variable_set('status_lights_image_path', $form_state['values']['image_path']);

  drupal_set_message('Configuration updated successfully.');
}

/**
 * Form builder for the status indicator global configuration form.
 *
 * @see status_lights_config_global_form_validate()
 * @see status_lights_config_global_form_submit()
 * @ingroup forms
 */
function status_lights_config_global_form()
{
  $path = variable_get('status_lights_image_path');
  $green_image = variable_get('status_lights_image_green');
  $yellow_image = variable_get('status_lights_image_yellow');
  $red_image = variable_get('status_lights_image_red');
  $redtime = variable_get('status_lights_red_time');
  $yellowtime = variable_get('status_lights_yellow_time');
  $scanpath = preg_replace('/http:\/\/[^\/]*/','',$path);
  $scanpath = substr($scanpath, 1, strlen($scanpath)-1);
  $files = file_scan_directory($scanpath, '/\.png|\.jpg|\.jpeg|.gif$/i');
  $options = array();
  foreach($files as $image)
  {
    $options[$image->filename] = "<img src='$path$image->filename'>";
  }

  $form = array();

  $form['image_path'] = array(
  	'#type'             =>      'textfield',
    '#title'            =>      t('Image Path'),
    '#default_value'    =>      $path,
    '#description'      =>      t('The absolute URI to the folder containing status light images (WITH trailing slash).'),
  );

  $form['yellow_time'] = array(
    '#type'             =>      'textfield',
    '#title'            =>      t('Yellow Status Time Limit'),
    '#default_value'    =>      $yellowtime,
    '#description'      =>      t('The time (in minutes) before closing that the indicator shows YELLOW'),
  );

  $form['red_time'] = array(
    '#type'             =>      'textfield',
    '#title'            =>      t('Red Status Time Limit'),
    '#default_value'    =>      $redtime,
    '#description'      =>      t('The time (in minutes) before closing that the indicator shows RED'),
  );

  $form['green_light'] = array(
    '#type'             =>      'radios',
    '#title'            =>      t('Green Image'),
    '#default_value'    =>      $green_image,
    '#options'          =>      $options,
    '#description'      =>      t('The image to use for a GREEN light (open)'),
  );

  $form['yellow_light'] = array(
    '#type'             =>      'radios',
    '#title'            =>      t('Yellow Image'),
    '#default_value'    =>      $yellow_image,
    '#options'          =>      $options,
    '#description'      =>      t('The image to use for a YELLOW light (closing soon)'),
  );

  $form['red_light'] = array(
    '#type'             =>      'radios',
    '#title'            =>      t('Red Image'),
    '#default_value'    =>      $red_image,
    '#options'          =>      $options,
    '#description'      =>      t('The image to use for a RED light (closed)'),
  );

  $form['submit'] = array(
    '#type'             =>      'submit',
    '#value'            =>      t('Save Configuration'),
  );

  return $form;
}

/**
 * Displays the main administrative page for indicators.
 *
 * Lists all indicators, enabled status, current status, and edit/delete
 * operations available.
 */
function status_lights_config_page()
{
  // we want to query our table for all indicators and display them with edit/delete options, and an ADD link at the top
  $indicators = db_select('status_indicator', 's')->fields('s')->orderBy('s.weight', 'ASC')->orderBy('s.name', 'ASC')->execute()->fetchAll();
  $out = "<table width='100%' cellpadding='3' cellspacing='1'>";
  $out .= "<tr>";
  $out .= "<th width='5%' colspan='2'></th><th>Indicator</th><th width='25%'>Operations</th>";
  $out .= "</tr>";
  foreach($indicators as $ind)
  {
    $status = (intval($ind->enabled) == 1) ? 'Disable' : 'Enable';
    $out .= "<tr style='border-bottom:solid 1px #bebfb9;'>";
    $out .= "<td><input type='checkbox' disabled name='indicator[]' value='$ind->iid'";
    $out .= (intval($ind->enabled)==1) ? ' checked ' : '';
    $out .= "></td>";
    $out .= "<td><img src='" . status_lights_get_status_image($ind->iid) . "'></td>";
    $out .= "<td>" . $ind->name;
    $out .= ($ind->description != NULL) ? "<br><em>$ind->description</em>" : '';
    $out .= "</td>";
    $out .= "<td><a href='statuslights/change/$ind->iid'>$status</a><span style='padding-left:10px;'><a href='statuslights/edit/$ind->iid'>Edit</a></span><span style='padding-left:10px;'><a href='statuslights/delete/$ind->iid'>Delete</a></span></td>";
    $out .= "</tr>";
  }
  $out .= "</table>";
  return $out;
}

/**
 * Helper function to get current status image.
 *
 * Returns the path to the image for the status of the specified indicator.
 * Checks the manual setting first, if manual setting is 0, it goes based off
 * the indicator's schedule.
 *
 * @param $iid
 *   The indicator ID number that the current status image is requested.
 *
 * @return
 *   Returns the absolute path to the image for the indicator's status.
 */
function status_lights_get_status_image($iid)
{
  $redtime = intval(variable_get('status_lights_red_time'));
  $yellowtime = intval(variable_get('status_lights_yellow_time'));
  $imagepath = variable_get('status_lights_image_path');
  $images[0] = '';
  $images[1] = variable_get('status_lights_image_green');
  $images[2] = variable_get('status_lights_image_yellow');
  $images[3] = variable_get('status_lights_image_red');
  return $imagepath . $images[status_lights_get_status($iid)];
}

function status_lights_get_status($iid)
{
  $utc = new DateTimeZone("UTC");
  $tz = new DateTimeZone(variable_get("date_default_timezone"));
  $redtime = intval(variable_get('status_lights_red_time'));
  $yellowtime = intval(variable_get('status_lights_yellow_time'));
  $result = db_select('status_indicator', 'A')->fields('A');
  $result->condition('A.iid', $iid);
  $result = $result->execute()->fetchAll();
  foreach($result as $indicator)
  {
    if($indicator->manual_status > 0)
    {
      // indicator was set manually, so only show that status level
      return $indicator->manual_status;
    } else
    {
      if(intval($indicator->global_time) != 1)
      {
        $yellowtime = intval($indicator->yellowtime);
        $redtime = intval($indicator->redtime);
      }
      // indicator is set to 0, so check based off information in the node type, field and taxonomy
      $results = db_select('field_data_field_building', 'A')->fields('A');
	  	$results->condition('field_building_tid', $indicator->taxonomy_term)->condition('bundle', $indicator->content_type);
	  	$results = $results->execute()->fetchAll();
      $like = new DateTime(null, $tz);
      $like = $like->format('Y-m-d');
      $status = 3;
      foreach($results as $nodes)
      {
        $table = 'field_data_' . $indicator->field;
        $daterows = db_select($table, 'A');
        $daterows->fields('A', array('entity_id', 'field_hours_value', 'field_hours_value2'))
        ->fields('B', array('field_status_value'))
        ->condition('A.bundle', $indicator->content_type)
        ->condition('A.entity_id', $nodes->entity_id)
        ->condition('A.field_hours_value', '%' . $like . '%', 'LIKE');
        $join = $daterows->leftJoin('field_data_field_status', 'B', 'A.entity_id = B.entity_id');
        $result = $daterows->execute()->fetchAll();
        foreach($result as $date)
        {
          $start = new DateTime($date->field_hours_value, $utc);
          $end = new DateTime($date->field_hours_value2, $utc);
          $mydate = new DateTime(null, $utc);
          switch ($date->field_status_value)
          {
            case 'open':
              if($mydate < $end && $mydate > $start)
              {
                $status = 1;
                $diff = $mydate->diff($end);
                $hours = $diff->h;
                $minutes = $diff->i + ($hours * 60);
                if($minutes <= $yellowtime && $minutes > 0)
                {
                  $status = 2;
                }
                if($minutes <= $redtime && $minutes > 0)
                {
                  $status = 3;
                }
              }
              else if ($mydate >= $end)
              {
                $status = 3;
              }
              break;
            case 'partial':
              if($mydate >= $start && $mydate <= $end)
              {
                $status = 3;
              }
              break;
            case 'closed':
              if($mydate >= $start)
              {
                $status = 3;
              }
              break;
          }
        }
      }
      return $status;
    }
  }
}

/**
 * Function that will retrieve the current day's hourly schedule.
 *
 * Returns a string for the specified indicator's schedule for the day.
 *
 * @param $iid
 *   The indicator ID number for the requested hourly schedule.
 *
 * @return
 *   Returns a string of the hours available for the day for the specified
 *   indicator.
 */
function status_lights_get_hours($iid)
{
  $utc = new DateTimeZone("UTC");
  $tz = new DateTimeZone(variable_get("date_default_timezone"));
  $result = db_select('status_indicator', 's')->fields('s')->condition('s.iid', $iid)->execute()->fetchAll();
  $dayopen = new DateTime(null, $utc);
  $dayopen->setTimeZone($tz);
  $dayopen->setTime(4,0,0);
  $dayclose = new DateTime(null, $utc);
  $dayclose->setTimeZone($tz);
  $dayclose->setTime(23,59,59);
  foreach($result as $indicator)
  {
    if(intval($indicator->show_hours) == 0)
    {
      return "";
    } else
    {
      $results = db_select('field_data_field_building', 'b')->fields('b')->condition('b.field_building_tid', $indicator->taxonomy_term)->condition('b.bundle', $indicator->content_type)->orderBy('b.entity_id', 'ASC')->execute()->fetchAll();
      $like = new DateTime(null, $tz);
      $like = $like->format('Y-m-d');
      foreach($results as $nodes)
      {
        $table = 'field_data_' . $indicator->field;
        $daterows = db_select($table, 'A');
        $daterows->fields('A', array('entity_id', $indicator->field . '_value', $indicator->field . '_value2'))
        ->fields('B', array('field_status_value'))
        ->condition('A.bundle', $indicator->content_type)
        ->condition('A.entity_id', $nodes->entity_id)
        ->condition('A.field_hours_value', '%' . $like . '%', 'LIKE');
        $join = $daterows->leftJoin('field_data_field_status', 'B', 'A.entity_id = B.entity_id');
        $result = $daterows->execute()->fetchAll();
        foreach($result as $date)
        {
          $start = new DateTime($date->field_hours_value, $utc);
          $start->setTimeZone($tz);
          $end = new DateTime($date->field_hours_value2, $utc);
          $end->setTimeZone($tz);
          switch ($date->field_status_value)
          {
            case 'open':
              if($start > $dayopen)
              {
                $dayopen = $start;
              }
              $dayclose = $end;
              break;
            case 'partial':
              break;
            case 'closed':
              if($start < $dayclose)
                $dayclose = $start;
              break;
          }
        }
      }
    }
  }
  $dayopen = $dayopen->format('h:i a');
  $dayclose = $dayclose->format('h:i a');
  if($dayclose == '12:00 am')
  {
    $dayclose = 'Midnight';
  }
  $status = status_lights_get_status($iid);
  $ret = "$dayopen to $dayclose";
  if($status == 3)
  {
    $ret = "Closed";
  }
  return $ret;
}
