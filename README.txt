-- SUMMARY --
The Status Lights module adds the ability to display dynamic images to represent the current status
 of a node based on a date field.  It was initially built to support building schedules and to
 display open and closed status lights of the building based on a set daily schedule.

-- INSTALLATION --
Install as normal. No special or additional steps required.

-- CONFIGURATION --
All global configuration settings can be accessed via the Configuration tab at the top of the page 
 at admin/structure/statuslights.  Settings include red, yellow and green images from the 
 module's directory (status_lights/images), or the directory that you set.

 Red Time indicates the time (in minutes) that must be met for a status indicator to be displayed
 as red.  Example, if the red time is 30 minutes, anything between 30 minutes to 0 minutes before 
 set end time will show as red.  If end time is 5:00PM, any time after 4:30 (inclusive), a red
 light will show.

 Yellow Time is just the same as red time, except a yellow light is shown.  Yellow Time is typically
 a larger value than Red Time, so that there is a window of yellow indicators prior to red.  An
 example would be 60 minutes.  Any time within the hour will show as yellow, with red time 
 overriding yellow status.

-- PERMISSIONS --
 'administer status light' permission is created and required for any user or role that wish to 
 add or modify existing status light, or alter the global configuration settings.

-- CONTACT --
Maintainers:
Joseph Pontani (jpontano) - http://drupal.org/user/1014606